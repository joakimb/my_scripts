#!/bin/sh
echo "Repo name: "
read name
echo "Username: "
read usr
read -s -p"Password: " pwd
git init

curl -k -X POST --user $usr:$pwd "https://api.bitbucket.org/1.0/repositories" -d "name=$name"
git remote add origin git@bitbucket.org:$usr/$name.git
echo "\nAdd all files? [y/n]"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) git add .; git commit -m "Initial commit."; git push --set-upstream origin master; break;;
        No ) break;;
    esac
done
echo "Done"
